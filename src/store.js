import {createStore} from "redux";
import reducers from "./reducers";
import initialState from "./state";

const configureStore = () => {
    return createStore(reducers);
};

export default configureStore;