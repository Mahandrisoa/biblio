import {combineReducers} from "redux";

import livreReducer from "../features/livre/livreReducer";
import membreReducer from "../features/membre/membreReducer";
import pretReducer from "../features/pret/pretReducer";

// return combined reducers
const reducers = combineReducers({
    livres: livreReducer,
    membres: membreReducer,
    prets: pretReducer
});

export default reducers;


