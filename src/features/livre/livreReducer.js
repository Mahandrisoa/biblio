import {ADD_LIVRE, UPDATE_LIVRE, DELETE_LIVRE} from "./types";

const initialState = {
    livres: [
        {
            numero: "L1",
            libelle: "Bear town",
            categorie: {label: "Kids", key: "Kids"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Frédérik Backman",
            edition: "Monaco",
            page: 150,
            image_path: require('../../assets/images/bear_town.png'),
            disponible: true
        },
        {
            numero: "L2",
            libelle: "Educated",
            categorie: {label: "Education & Reference", key: "Edu&Reference"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Tara vecton",
            edition: "A MEMOIR",
            page: 260,
            image_path: require('../../assets/images/educated.png'),
            disponible: true
        },
        {
            numero: "L3",
            libelle: "Hotel Corner of Bitter Sweet",
            categorie: {label: "Business", key: "Business"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Jamie ford",
            edition: "Travel",
            page: 260,
            image_path: require('../../assets/images/hotel_corner.png'),
            disponible: true
        },
        {
            numero: "L4",
            libelle: "How to walk away",
            categorie: {label: "Education & Reference", key: "Edu&Reference"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Katherine Center",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/how_to_walk_away.png'),
            disponible: false
        },
        {
            numero: "L5",
            libelle: "Still Me",
            categorie: {label: "Health & Fitness", key: "Health&Fitness"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Katherine Center",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/still_me.png'),
            disponible: false
        },
        {
            numero: "L6",
            libelle: "The Best book in 2018",
            categorie: {label: "Art et music", key: "Art&music"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Author",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/the_best_book.png'),
            disponible: false
        },
        {
            numero: "L7",
            libelle: "The Hate you give",
            categorie: {label: "Horror", key: "Horror"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Angie Thomas",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/the_hate_you_give.png'),
            disponible: false
        },
        {
            numero: "L8",
            libelle: "The sun does shine",
            categorie: {label: "Horror", key: "Horror"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Anthony Ray hitton",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/the_sun.png'),
            disponible: false
        },
        {
            numero: "L9",
            libelle: "Us against you",
            categorie: {label: "Horror", key: "Horror"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Frederik Backman",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/default_book.png'),
            disponible: false
        },
    ],
    categories: [
        {label: "Art et music", key: "Art&music"},
        {label: "Biographies", key: "Biographies"},
        {label: "Business", key: "Business"},
        {label: "Kids", key: "Kids"},
        {label: "Comics", key: "Comics"},
        {label: "Computer & Tech", key: "Computer&Tech"},
        {label: "Cooking", key: "Cooking"},
        {label: "Hobbies & Craft", key: "Hobbies&Craft"},
        {label: "Education & Reference", key: "Edu&Reference"},
        {label: "Health & Fitness", key: "Health&Fitness"},
        {label: "History", key: "History"},
        {label: "Home & Garden", key: "Home&Garden"},
        {label: "Horror", key: "Horror"},
        {label: "Entertainment", key: "Entertainment"},
    ]
};

const livreReducer = (state = initialState, action) => {
    let livres = [...state.livres];
    let newState;
    switch (action.type) {
        case ADD_LIVRE:
            livres.push(action.payload);
            newState = {...state, livres};
            return newState;
            break;
        case UPDATE_LIVRE:
            const {numero} = action.payload;
            let index = livres.findIndex((l) => l.numero === numero);
            livres[index] = {...action.payload};
            newState = {...state, livres};
            return newState;
            break;
        case DELETE_LIVRE:
            const selected = action.payload;
            let temp = state.livres;
            selected.map((s, i) => {
                temp = temp.filter(l => l.numero !== s.numero);
            });
            livres = temp;
            newState = {...state, livres};
            return newState;
            break;
        default:
            return state;
    }
};

export default livreReducer;