import React from "react";
import {TextInput} from "react-native";
import {
    Body,
    Button,
    Container,
    Content, DatePicker,
    Form,
    Header,
    Icon,
    Input,
    Item,
    Label,
    Left, Picker,
    Right, Textarea,
    Title
} from "native-base";
import {updateLivre} from "./livres";
import {connect} from "react-redux";

const LIST_LIVRES = "ListLivre";

class UpdateLivre extends React.Component {
    constructor(props) {
        super(props);
        const book = this.props.livres.find(l => l.selected); // get selected book
        this.state = book; // set initial state
        this.setDate = this.setDate.bind(this);
    }

    onCategorieValueChange(categorie) {
        this.setState({categorie});
    }

    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }

    setDate(date_publication) {
        this.setState({date_publication});
    }

    navigateTo(route: string) {
        this.props.navigation.navigate(route);
    }

    submitForm() {
        const book = { numero, libelle, categorie, description, date_publication, auteur, edition, page} = this.state;
        this.props.update(book);
        this.navigateTo(LIST_LIVRES);
    }

    componentDidMount() {
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.navigateTo(LIST_LIVRES)}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Modifier un livre</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.submitForm()}>
                            <Icon name='ios-checkmark'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                        <Item inlineLabel>
                            <Label>Nom du livre</Label>
                            <Input onChangeText={(libelle) => this.setState({libelle})}
                                   value={this.state.libelle}/>
                        </Item>
                        <Item inlineLabel last>
                            <Label>Catégorie</Label>
                            <Picker
                                mode="dropdown"
                                style={{}}
                                placeholder="Selectionnez la catégorie"
                                placeholderStyle={{color: "#bfc6ea"}}
                                placeholderIconColor="#007aff"
                                selectedValue={this.state.categorie}
                                onValueChange={this.onCategorieValueChange.bind(this)}
                            >
                                {this.props.categories.map((item, index) =>
                                    <Picker.Item label={item.label} value={item.key} key={index}/>
                                )}

                            </Picker>
                        </Item>
                        <Item inlineLabel last>
                            <Label>Date de publication</Label>
                            <DatePicker
                                defaultDate={new Date(this.state.date_publication)}
                                minimumDate={new Date(1990, 1, 1)}
                                maximumDate={new Date(2050, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Select date"
                                textStyle={{color: "green"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                        <Item inlineLabel last>
                            <Label>Auteur</Label>
                            <Input value={this.state.auteur}
                                   onChangeText={(auteur) => this.setState({auteur})}/>
                        </Item>
                        <Item inlineLabel>
                            <Label>Edition</Label>
                            <Input value={this.state.edition}
                                   onChangeText={(edition) => this.setState({edition})}/>
                        </Item>
                        <Item inlineLabel>
                            <Label>Nombre de pages</Label>
                            <Input value={this.state.page}
                                   onChangeText={(page) => this.setState({page})}/>
                        </Item>
                        <Item>
                            <Textarea rowSpan={5} keyboardType="phone-pad" placeholder="Résumé du livre "
                                      style={{width: '100%'}}
                                      value={this.state.resume}
                                      onChangeText={(resume) => this.setState({resume})}/>
                        </Item>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
        categories: state.livres.categories
    };
};

const mapDispatchToProps = dispatch => {
    return {
        update: (livre) => {
            dispatch(updateLivre(livre));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateLivre);