import {ADD_LIVRE, UPDATE_LIVRE, DELETE_LIVRE} from './types';

const addLivre = livre => {
    return {
        type: ADD_LIVRE,
        payload: livre
    }
};

const updateLivre = livre => {
    return {
        type: UPDATE_LIVRE,
        payload: livre
    }
};

const deleteLivre = livres => {
    return {
        type: DELETE_LIVRE,
        payload: livres
    }
};

export {
    addLivre,
    updateLivre,
    deleteLivre
};