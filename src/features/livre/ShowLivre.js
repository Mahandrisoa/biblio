import React from "react";
import {Image, Text, View, ImageBackground, ScrollView} from "react-native";
import {H1, H3, Thumbnail, Button, Title, Left, Icon, Body, Right, Header, Content} from "native-base";
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {connect} from "react-redux";

class ShowLivre extends React.Component {
    constructor(props) {
        super(props);
        const book = this.props.livres.find(l => l.selected); // get selected book
        this.state = book;
    }

    render() {
        const image = {
            image_path: require('../../assets/images/livre.png'),
            description: 'Hi My name is John, I\'m a creative geek ' +
                'from San francisco, CA. I enjoy creating eye candy ' +
                'solutions for web and mobile apps. Contact me at @john.com'
        };
        return (
            <ImageBackground source={require('../../assets/images/fond_list_livre.png')}
                             style={{width: '100%', height: '100%'}}>
                <Header transparent searchbar>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title></Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='search'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <H1></H1>
                    <View style={{height: hp('50%'), justifyContent: 'center', alignItems: 'center'}}>
                        <Image source={image.image_path} style={{width: '100%', height: '100%'}}></Image>
                    </View>
                    <Text style={{color: 'white', fontWeight: 'bold', margin: 20}}>

                    </Text>
                    <View style={{
                        flexDirection: 'column',
                        height: hp('25%'),
                        margin: 20,
                        padding: 20,
                        backgroundColor: '#BEBEC4'
                    }}>
                        <H3 style={{color: 'white', fontWeight: 'bold'}}>Livres Similaires</H3>
                        <ScrollView style={{padding: 10}} horizontal>
                            <Thumbnail square large source={image.image_path}/>
                            <Thumbnail square large source={image.image_path}/>
                            <Thumbnail square large source={image.image_path}/>
                            <Thumbnail square large source={image.image_path}/>
                            <Thumbnail square large source={image.image_path}/>
                            <Thumbnail square large source={image.image_path}/>
                        </ScrollView>
                    </View>
                </Content>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowLivre);