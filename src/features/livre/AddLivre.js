import React from "react";
import {
    Body,
    Button,
    Container,
    Header,
    Label,
    Icon,
    DatePicker,
    Left,
    Right,
    Title,
    Content,
    Picker,
    Form,
    Item,
    FlatList,
    Input, Textarea
} from "native-base";
import {Alert} from "react-native";
import {connect} from "react-redux";
import {addLivre, updateLivre, deleteLivre} from "./livres";

class AddLivre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            numero: '',
            libelle: '',
            categorie: 'Art&music',
            auteur: '',
            edition: '',
            page: '',
            resume: '',
            date_publication: new Date(),
        };
        this.setDate = this.setDate.bind(this);
    }

    onCategorieValueChange(categorie) {
        this.setState({categorie});
    }

    setDate(date_publication) {
        this.setState({date_publication});
    }

    submitForm() {
        let livre = { libelle, categorie, description, date_publication, auteur, edition, page} = this.state;
        livre.numero = `L${this.props.livres.length+1}`;

        this.props.add({
            ...livre,
            image_path: require('../../assets/images/default_book.png')
        });
        this.props.navigation.goBack();
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Ajouter un livre</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.submitForm()}>
                            <Icon name='ios-checkmark'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                        <Item inlineLabel>
                            <Label>Nom du livre</Label>
                            <Input onChangeText={(libelle) => this.setState({libelle})}
                                   value={this.state.libelle}/>
                        </Item>
                        <Item inlineLabel last>
                            <Label>Catégorie</Label>
                            <Picker
                                mode="dropdown"
                                style={{}}
                                placeholder="Selectionnez la catégorie"
                                placeholderStyle={{color: "#bfc6ea"}}
                                placeholderIconColor="#007aff"
                                selectedValue={this.state.categorie}
                                onValueChange={this.onCategorieValueChange.bind(this)}
                            >
                                {this.props.categories.map((item, index) =>
                                    <Picker.Item label={item.label} value={item.key} key={index}/>
                                )}

                            </Picker>
                        </Item>
                        <Item inlineLabel last>
                            <Label>Date de publication</Label>
                            <DatePicker
                                defaultDate={new Date()}
                                minimumDate={new Date(1990, 1, 1)}
                                maximumDate={new Date(2050, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Select date"
                                textStyle={{color: "green"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                        <Item inlineLabel last>
                            <Label>Auteur</Label>
                            <Input value={this.state.auteur}
                                   onChangeText={(auteur) => this.setState({auteur})}/>
                        </Item>
                        <Item inlineLabel>
                            <Label>Edition</Label>
                            <Input value={this.state.edition}
                                   onChangeText={(edition) => this.setState({edition})}/>
                        </Item>
                        <Item inlineLabel>
                            <Label>Nombre de pages</Label>
                            <Input value={this.state.page}
                                   onChangeText={(page) => this.setState({page})}/>
                        </Item>
                        <Item>
                            <Textarea rowSpan={5} placeholder="Résumé du livre "
                                      style={{width: '100%'}}
                                      value={this.state.resume}
                                      onChangeText={(resume) => this.setState({resume})}/>
                        </Item>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
        categories: state.livres.categories
    }
};

const mapDispatchToProps = dispatch => {
    return {
        add: (livre) => {
            dispatch(addLivre(livre));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddLivre);
