import React from "react";
import {
    View,
    Image,
    ScrollView,
    ImageBackground,
    Text,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import Modal from "react-native-modal";
import {
    Title, Fab,
    Container,
    Header,
    Content,
    Body,
    Left,
    Right,
    Icon,
    Button,
    Thumbnail,
    ListItem,
    Item,
    List,
    Form,
    DatePicker,
    Picker
} from "native-base";
import {connect} from "react-redux";
import {deleteLivre, updateLivre} from "./livres";
import {addPret} from "../pret/prets";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";

class ListLivre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            canEdit: false,
            canDelete: false,
            selectMode: false,
            isModalVisible: false,
            selected: "key3",
            chosenDate: new Date(),
            membre: this.props.membres[0]
        };
        this.setDate = this.setDate.bind(this);
        this.clearSelection();
    }

    setDate(newDate) {
        this.setState({chosenDate: newDate});
    }

    componentDidMount(): void {
        this.clearSelection();
    }

    clearSelection() {
        this.props.livres.filter(l => l.selected === true).map(sl => sl.selected = false);
    }

    _longPressLivre(index) {
        let _livres = [...this.props.livres];
        _livres[index].selected = true;
        if (_livres[index].disponible || _livres[index].selected) {
            let count = this.selectedLivres(_livres);
            let obj = {
                selectMode: true
            };
            if (count === 1) {
                obj.canEdit = true;
            } else {
                if (count > 1) {
                    obj.canEdit = false;
                }
            }
            this.setState(obj);
        }
    }

    _pressLivre(index) {
        let selectMode = this.state.selectMode;
        if (selectMode) {
            let _livres = [...this.props.livres];
            if (_livres[index].disponible) {
                _livres[index].selected = _livres[index].selected ? false : true;
                let count = this.selectedLivres(_livres);
                let obj = {};
                if (count === 1) {
                    obj.canEdit = true;
                } else {
                    if (count > 1) {
                        obj.canEdit = false;
                    }
                }
                this.setState(obj);
            }
        }
    }

    _deleteLivres() {
        const livres = this.props.livres.filter(l => l.selected === true);
        this.props.delete(livres);
        this._cancelSelection();
    }

    _cancelSelection() {
        let _livres = [...this.props.livres];
        _livres.map(livre => {
            if (livre.selected) livre.selected = false;
        });
        this.setState({
            livres: _livres,
            selectMode: false
        })
    }

    selectedLivres(livres) {
        return livres.filter(livre => livre.selected === true).length;
    }

    gotoUpdate() {
        this.props.navigation.navigate('UpdateLivre');
    }

    // method to handle how to render Fab
    _setModalVisible(visible) {
        this.setState({isModalVisible: visible});
    }

    _toggleModal = () => {
        this.setState({isModalVisible: !this.state.isModalVisible});
    };

    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }

    preter() {
        let livres = this.props.livres.filter(livre => livre.selected === true);
        let membre = this.state.membre;
        let date_pret = this.state.chosenDate;
        let pret = {
            livres: livres,
            membre: membre,
            date_pret: date_pret,
            disponible: false
        };
        this.props.add_pret(pret);
        this.props.set_disponible(livres, false);
        this.setState({
            "isModalVisible": false
        });
        this._cancelSelection();
        this.clearSelection();
        this.props.navigation.navigate('ListPret');
    }

    onMembreChange(membre) {
        this.setState({membre});
    }

    renderModalContent() {
        return (
            <View style={styles.content}>
                <View style={{height: 40, flexDirection: "row"}}>
                    <Left>
                        <Text style={styles.contentTitle}>
                            {this.props.livres.filter(l => l.selected === true).length} Livres
                        </Text>
                    </Left>
                    <Right>
                        <Item>
                            <DatePicker
                                defaultDate={new Date()}
                                minimumDate={new Date(2018, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Date de prêt"
                                textStyle={{color: "black"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                    </Right>
                </View>
                <View>
                    <ScrollView horizontal>
                        <View style={{
                            width: wp("100%"),
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "flex-start"
                        }}>
                            {this.props.livres.filter(l => l.selected === true).map((livre, index) => (
                                    <TouchableOpacity key={index}
                                                      style={{margin: 10, padding: 0}}
                                    >
                                        <Thumbnail square large source={livre.image_path}/>
                                    </TouchableOpacity>
                                )
                            )}
                        </View>
                    </ScrollView>
                </View>
                <Form>
                    <Item>
                        <Picker
                            mode="dropdown"
                            style={{}}
                            placeholder="Selectionnez le membre"
                            placeholderStyle={{color: "#bfc6ea"}}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.membre}
                            onValueChange={this.onMembreChange.bind(this)}
                        >
                            {this.props.membres.map((item, index) =>
                                <Picker.Item label={item.nom + ' ' + item.prenom} value={JSON.stringify(item)}
                                             key={item.numero}/>
                            )}

                        </Picker>
                    </Item>
                </Form>
                <Button full rounded info
                        onPress={() => this.preter()}>
                    <Text style={{color: "white", fontWeight: "bold"}}>Prêter</Text>
                </Button>
            </View>
        );
    }

    _navigateHome() {
        this.props.navigation.navigate('HomePage');
    }

    render() {
        return (
            <Container>
                <ImageBackground source={require('../../assets/images/fond_list_livre.png')}
                                 style={styles.backgroundImage}>
                    {!this.state.selectMode &&
                    <Header transparent searchbar>
                        <Left>
                            <Button transparent onPress={() => this._navigateHome()}>
                                <Icon name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                            <Title>Liste des livres</Title>
                        </Body>
                        <Right>
                            <Button transparent>
                                <Icon name='search'/>
                            </Button>
                        </Right>
                    </Header>}
                    {/*Cancel header */}
                    {this.state.selectMode &&
                    <Header transparent searchbar>
                        <Left>
                            <Button transparent onPress={() => this._cancelSelection()}>
                                <Icon name='ios-close'/>
                            </Button>
                        </Left>
                        <Body>
                            <Title>Liste des livres</Title>
                        </Body>
                        <Right>
                            {this.state.canEdit &&
                            <Button transparent onPress={() => this.gotoUpdate()}>
                                <Icon name='ios-color-palette'/></Button>}
                            <Button transparent onPress={() => this._deleteLivres()}>
                                <Icon name='trash'/>
                            </Button>
                        </Right>
                    </Header>}
                    <Content>
                        <List>
                            <ListItem itemDivider>
                                <Left>
                                    <Text style={{color: '#c41d56'}}>Toutes les livres</Text>
                                </Left>
                            </ListItem>
                            <ListItem style={{flexWrap: "wrap", justifyContent: "center"}}>
                                {this.props.livres.map((livre, index) => (
                                        <TouchableOpacity key={index}
                                                          onPress={() => this._pressLivre(index)}
                                                          onLongPress={() => this._longPressLivre(index)}
                                                          style={{
                                                              padding: 0,
                                                              opacity: livre.disponible || livre.selected ? 1 : 0.1,
                                                              justifyContent: 'center',
                                                              alignItems: 'flex-start',
                                                              borderColor: livre.selected ? '#008000' : "",
                                                              borderWidth: livre.selected ? 2 : 0,
                                                              margin: 5,
                                                          }}
                                        >
                                            <Image source={livre.image_path} style={{width: wp('27%'), height: wp('32%')}}/>
                                            <Text style={{
                                                fontSize: 14,
                                                overflow: "hidden",
                                                color: "white",
                                                width: wp('27%')
                                            }}>{livre.libelle}</Text>
                                        </TouchableOpacity>
                                    )
                                )}
                            </ListItem>
                        </List>
                    </Content>
                    {!this.state.selectMode &&
                    <Fab
                        active={true}
                        direction="up"
                        containerStyle={{}}
                        style={{backgroundColor: '#5067FF'}}
                        position="bottomRight"
                        onPress={() => this.props.navigation.navigate('AddLivre')}>
                        <Icon name="add"/>
                    </Fab>
                    }
                    {this.state.selectMode &&
                    <Fab
                        active={true}
                        direction="up"
                        containerStyle={{}}
                        style={{backgroundColor: '#5067FF'}}
                        position="bottomRight"
                        onPress={() => this.setState({isModalVisible: true})}
                    >
                        < Icon name="flask"/>
                    </Fab>
                    }

                </ImageBackground>
                <Modal
                    isVisible={this.state.isModalVisible}
                    onSwipeComplete={() => this.setState({isModalVisible: false})}
                    swipeDirection={['up', 'left', 'right', 'down']}
                    style={styles.bottomModal}
                >
                    {this.renderModalContent()}

                </Modal>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: '100%',
        height: '100%'
    },
    content: {
        padding: 22,
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',
        borderWidth: 3,
        backgroundColor: "white"
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12,
        margin: 10
    },
    bottomModal: {
        margin: 0,
        justifyContent: "flex-end"
    }
});

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
        prets: state.prets.prets,
        membres: state.membres.membres
    };
};

const mapDispatchToProps = dispatch => {
    return {
        delete: (livres: array) => {
            dispatch(deleteLivre(livres));
        },
        add_pret: (pret) => {
            dispatch(addPret(pret));
        },
        set_disponible: (livres: array, value) => {
            livres.map(livre => {
                livre.disponible = value;
                dispatch(updateLivre(livre));
            })
        },
        set_disponibleLivre: (livre: array, value) => {
            dispatch(updateLivre(livre));
        },

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListLivre);