import React, {Component} from "react";
import Table from "react-native-simple-table";
import {Platform, StyleSheet, Text, View} from "react-native";
import {Container, Header, Content, Left, Button, Icon, Body, Title} from "native-base";
import {connect} from "react-redux";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {PieChart} from "react-native-chart-kit";
import moment from "moment";

const columns = [
    {
        title: 'N°',
        dataIndex: 'numero',
        width: wp('15%')
    },
    {
        title: 'Design',
        dataIndex: 'libelle',
        width: wp('30%')
    },
    {
        title: 'Auteur',
        dataIndex: 'auteur',
        width: wp('20%')
    },
    {
        title: 'Date Edition',
        dataIndex: 'date_publication',
        width: wp('20%')
    },
    {
        title: 'Etat',
        dataIndex: 'disponible',
        width: wp('15%')
    }
];

class Situation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: this.generateContent()
        };
    }

    generateContent() {
        return this.props.livres.map((value, index) => {
            return {
                numero: value.numero,
                libelle: value.libelle,
                auteur: value.auteur,
                date_publication: moment(value.date_publication).format('DD-MM-YYYY'),
                disponible: value.disponible ? "Oui" : "Non"
            }
        });
    }

    componentDidMount(): void {
    }

    _navigateHome() {
        this.props.navigation.navigate('HomePage');
    }

    render() {
        let disponibles = this.props.livres.filter(l => l.disponible === true).length;
        let non_disponible = this.props.livres.filter(l => l.disponible === false).length;
        let pieChartData = [{
            name: 'Disponible',
            population: disponibles,
            color: '#008000',
            legendFontColor: '#7F7F7F',
            legendFontSize: 15
        },
            {
                name: 'Résérvé',
                population: non_disponible,
                color: '#891b19',
                legendFontColor: '#7F7F7F',
                legendFontSize: 15
            }];

        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this._navigateHome()}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Situation des Livres</Title>
                    </Body>
                </Header>
                <Content>
                    <Table height={hp('50%')} columnWidth={60} columns={columns} dataSource={this.state.dataSource}/>
                    <PieChart
                        data={pieChartData}

                        height={hp('30%')}
                        width={wp('100%')}
                        chartConfig={{
                            backgroundColor: 'white',
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            }
                        }}
                        accessor="population"
                        style={{
                            marginVertical: 8,
                            backgroundColor: '#26872a',
                            backgroundGradientFrom: '#43a047',
                            backgroundGradientTo: '#66bb6a',
                            style: {
                                borderRadius: 16
                            }
                        }}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...Platform.select({
            ios: {
                paddingTop: 20
            },
            android: {}
        }),
    },
    title: {
        fontSize: 18,
        padding: 10,
        textAlign: 'center'
    }
});

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Situation);