import React from "react";
import {View, Text, Image, ScrollView, StyleSheet, FlatList, TouchableOpacity} from "react-native";
import {
    Container,
    Header,
    Icon,
    Button,
    Content,
    CardItem,
    Card,
    Body,
    Left, Title, Thumbnail, Fab, Right, Item, DatePicker, Form, Picker, H3,
} from "native-base";
import {addPret, deletePret, updatePret} from "./prets";
import {connect} from "react-redux";
import Modal from "react-native-modal";
import {updateLivre} from "../livre/livres";
import moment from "moment";

class ListPret extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectMode: false,
            canEdit: false,
            canDelete: false,
            isModalVisible: false,
            membre: this.props.membres[0],
            isModalUpdateVisible: false
        };
    }

    _longPressPret(index) {
        let _prets = [...this.props.prets];
        _prets[index].selected = true;
        let count = this.selectedPrets(_prets);
        let obj = {
            selectMode: true
        };
        if (count === 1) {
            obj.canEdit = true;
            obj.canDelete = true;
        } else {
            if (count > 1) {
                obj.canDelete = true;
                obj.canEdit = false;
            } else {
                if (count === 0) {
                    obj.canDelete = false;
                    obj.canEdit = false;
                }
            }
        }
        this.setState(obj);
    }

    _pressPret(index) {
        let selectMode = this.state.selectMode;
        if (selectMode) {
            let _prets = [...this.props.prets];
            _prets[index].selected = !_prets[index].selected;
            let count = this.selectedPrets(_prets);
            let obj = {};
            if (count === 1) {
                obj.canEdit = true;
                obj.canDelete = true;
            } else {
                if (count > 1) {
                    obj.canDelete = true;
                    obj.canEdit = false;
                } else {
                    if (count === 0) {
                        obj.canDelete = false;
                        obj.canEdit = false;
                    }
                }
            }
            this.setState(obj);
            console.log(this.selectedPrets(this.props.prets));
        }
    }

    onMembreChange(membre) {
        this.setState({membre});
    }

    selectedPrets(prets) {
        return prets.filter(p => p.selected === true).length;
    }

    renderModalContent() {
        return (
            <View style={styles.content}>
                <View style={{height: 40, flexDirection: "row"}}>
                    <Left>
                        <Text style={styles.contentTitle}>
                            {this.props.livres.filter(l => l.selected === true).length} Livres
                        </Text>
                    </Left>
                    <Right>
                        <Item>
                            <DatePicker
                                defaultDate={new Date()}
                                minimumDate={new Date(2018, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Date de prêt"
                                textStyle={{color: "black"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                    </Right>
                </View>
                <View>
                    <ScrollView horizontal>
                        <View style={{
                            width: "100%",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "flex-start"
                        }}>
                            {this.props.livres.filter(l => l.selected === true).map((livre, index) => (
                                    <TouchableOpacity key={index}
                                                      style={{margin: 10, padding: 0}}
                                    >
                                        <Thumbnail square large source={livre.image_path}/>
                                    </TouchableOpacity>
                                )
                            )}
                        </View>
                    </ScrollView>
                </View>
                <Form>
                    <Item>
                        <Picker
                            mode="dropdown"
                            style={{}}
                            placeholder="Selectionnez le membre"
                            placeholderStyle={{color: "#bfc6ea"}}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.membre}
                            onValueChange={this.onMembreChange.bind(this)}
                        >
                            {this.props.membres.map((item, index) =>
                                <Picker.Item label={item.nom + ' ' + item.prenom} value={JSON.stringify(item)}
                                             key={item.numero}/>
                            )}

                        </Picker>
                    </Item>
                </Form>
                <Button full rounded info
                        onPress={() => this.preter()}>
                    <Text style={{color: "white", fontWeight: "bold"}}>Prêter</Text>
                </Button>
            </View>
        );
    }

    convertPretArrayToMap(prets) {
        var pretDateMap = {}; // Create the blank map
        prets.forEach(function (pretItem) {
            if (!pretDateMap[pretItem.date_pret]) {
                // Create an entry in the map for the category if it hasn't yet been created
                pretDateMap[pretItem.date_pret] = [];
            }
            pretDateMap[pretItem.date_pret].push(pretItem);
        });
        return pretDateMap;
    }

    _deletePrets() {
        const prets = this.props.prets.filter(l => l.selected === true);
        this.props.delete(prets);
        prets.map(p => this.props.set_disponible(p.livres, true));
        this._cancelSelection();
    }

    _cancelSelection() {
        let _prets = [...this.props.prets];
        _prets.map(p => {
            if (p.selected) p.selected = false;
        });
        this.setState({
            prets: _prets,
            selectMode: false
        });
    }

    clearSelection() {
        let _prets = [...this.props.prets];
        this.props.prets.map(pret => {
            if (pret.selected) pret.selected = false;
        });
    }

    componentDidMount(): void {
        this.clearSelection();
        console.log("mount : ", this.selectedPrets(this.props.prets));
    }

    _navigateHome() {
        this.props.navigation.navigate('HomePage');
    }

    gotoUpdate() {
        this.props.navigation.navigate('UpdatePret');
    }

    render() {
        return (
            <Container>
                {!this.state.selectMode &&
                <Header searchbar>
                    <Left>
                        <Button transparent onPress={() => this._navigateHome()}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Liste des prêts</Title>
                    </Body>
                </Header>}
                {this.state.selectMode &&
                <Header searchbar>
                    <Left>
                        <Button transparent onPress={() => this._cancelSelection()}>
                            <Icon name='ios-close'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Liste des prets</Title>
                    </Body>
                    <Right>
                        {this.state.canEdit &&
                        <Button transparent onPress={() => this.gotoUpdate()}>
                            <Icon name='ios-color-palette'/></Button>
                        }
                        {this.state.canDelete &&
                        <Button transparent onPress={() => this._deletePrets()}>
                            <Icon name='trash'/>
                        </Button>
                        }
                    </Right>
                </Header>}
                <Content padder>
                    {this.props.prets.map((item, index) =>
                        (<TouchableOpacity key={index}
                                           style={{borderColor: item.selected ? "green" : "white", borderWidth: 2}}
                                           onPress={() => this._pressPret(index)}
                                           onLongPress={() => this._longPressPret(index)}>
                            <Card pointerEvents="none">
                                <CardItem header>
                                    <Left>
                                        <Thumbnail source={item.membre.avatar}/>
                                        <Body>
                                            <Text>{item.membre.nom} {item.membre.prenom}</Text>
                                            <Text note>Prêt
                                                du {moment(item.date_pret).format('DD-MM-YYYY')}
                                            </Text>
                                        </Body>
                                    </Left>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <ScrollView horizontal>
                                            <View style={{
                                                width: "100%",
                                                flexDirection: "row",
                                                flexWrap: "wrap",
                                                justifyContent: "flex-start"
                                            }}>
                                                {item.livres.map((livre, index) => (
                                                        <TouchableOpacity key={index}
                                                                          style={{margin: 10, padding: 0}}
                                                        >
                                                            <Thumbnail square large source={livre.image_path}/>
                                                        </TouchableOpacity>
                                                    )
                                                )}
                                            </View>
                                        </ScrollView>
                                    </Body>
                                </CardItem>
                            </Card>
                        </TouchableOpacity>)
                    )}
                </Content>

                <Modal
                    isVisible={this.state.isModalVisible}
                    onSwipeComplete={() => this.setState({isModalVisible: null})}
                    swipeDirection={['up', 'left', 'right', 'down']}
                    style={styles.bottomModal}
                >
                    {this.renderModalContent()}

                </Modal>
                <Fab
                    active={true}
                    direction="up"
                    containerStyle={{}}
                    style={{backgroundColor: '#5067FF'}}
                    position="bottomRight"
                    onPress={() => {
                        this.props.navigation.navigate('AddPret')
                    }}>
                    <Icon name="add"/>
                </Fab>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: '100%',
        height: '100%'
    },
});

const mapStateToProps = state => {
    return {
        prets: state.prets.prets,
        livres: state.livres.livres,
        membres: state.membres.membres
    };
};

const mapDispatchToProps = dispatch => {
    return {
        delete: (prets: array) => {
            dispatch(deletePret(prets));
        },
        set_disponible: (livres: array, value) => {
            livres.map(livre => {
                livre.disponible = value;
                dispatch(updateLivre(livre));
            })
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListPret);