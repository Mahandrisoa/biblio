import {ADD_PRET, UPDATE_PRET, DELETE_PRET} from "./types";

const initialState = {
    prets: [
        {
            membre: {
                nom: "Richester",
                prenom: "Fabrice",
                numero: "M1",
                date_naissance: "30-07-1997",
                description: "Aucune",
                avatar: require('../../assets/images/fabrice.png')
            },
            livres: [
                {
                    numero: "L4",
                    libelle: "How to walk away",
                    categorie: {label: "Education & Reference", key: "Edu&Reference"},
                    description: "",
                    date_publication: "2019-02-01",
                    auteur: "Katherine Center",
                    edition: "A novel",
                    page: 260,
                    image_path: require('../../assets/images/how_to_walk_away.png'),
                    selected: true
                },
                {
                    numero: "L5",
                    libelle: "Still Me",
                    categorie: {label: "Health & Fitness", key: "Health&Fitness"},
                    description: "",
                    date_publication: "2019-02-01",
                    auteur: "Katherine Center",
                    edition: "A novel",
                    page: 260,
                    image_path: require('../../assets/images/still_me.png'),
                    selected: true
                },
                {
                    numero: "L6",
                    libelle: "The Best book in 2018",
                    categorie: {label: "Art et music", key: "Art&music"},
                    description: "",
                    date_publication: "2019-02-01",
                    auteur: "Author",
                    edition: "A novel",
                    page: 260,
                    image_path: require('../../assets/images/the_best_book.png')
                },
            ],
            date_pret: "2019-03-03",
            selected: false,
        },
        {
            membre: {
                nom: "Sharon",
                prenom: "Alice",
                numero: "M2",
                date_naissance: "30-07-1997",
                description: "Aucune",
                avatar: require('../../assets/images/alice.jpg')
            },
            livres: [
                {
                    numero: "L7",
                    libelle: "The Hate you give",
                    categorie: {label: "Horror", key: "Horror"},
                    description: "",
                    date_publication: "2019-02-01",
                    auteur: "Angie Thomas",
                    edition: "A novel",
                    page: 260,
                    image_path: require('../../assets/images/the_hate_you_give.png')
                },
                {
                    numero: "L9",
                    libelle: "Us against you",
                    categorie: {label: "Horror", key: "Horror"},
                    description: "",
                    date_publication: "2019-02-01",
                    auteur: "Frederik Backman",
                    edition: "A novel",
                    page: 260,
                    image_path: require('../../assets/images/us_against_you.png')
                }
            ],
            date_pret: "2019-03-02",
            selected: false,
        },
        {
            membre: {
                nom: "Beaulieu",
                prenom: "Didier",
                numero: "M1",
                date_naissance: "30-07-1997",
                description: "Aucune",
                avatar: require('../../assets/images/didier.jpg')
            },
            livres: [
                {
                    numero: "L8",
                    libelle: "The sun does shine",
                    categorie: {label: "Horror", key: "Horror"},
                    description: "",
                    date_publication: "2019-02-01",
                    auteur: "Anthony Ray hitton",
                    edition: "A novel",
                    page: 260,
                    image_path: require('../../assets/images/the_sun.png')
                }
            ],
            date_pret: "2019-03-09",
            selected: false,
        }
    ],
    livres: [
        {
            numero: "L1",
            libelle: "Bear town",
            categorie: {label: "Kids", key: "Kids"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Frédérik Backman",
            edition: "Monaco",
            page: 150,
            image_path: require('../../assets/images/bear_town.png'),
            disponible: true
        },
        {
            numero: "L2",
            libelle: "Educated",
            categorie: {label: "Education & Reference", key: "Edu&Reference"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Tara vecton",
            edition: "A MEMOIR",
            page: 260,
            image_path: require('../../assets/images/educated.png'),
            disponible: true
        },
        {
            numero: "L3",
            libelle: "Hotel Corner of Bitter Sweet",
            categorie: {label: "Business", key: "Business"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Jamie ford",
            edition: "Travel",
            page: 260,
            image_path: require('../../assets/images/hotel_corner.png'),
            disponible: true
        },
        {
            numero: "L4",
            libelle: "How to walk away",
            categorie: {label: "Education & Reference", key: "Edu&Reference"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Katherine Center",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/how_to_walk_away.png'),
            disponible: false
        },
        {
            numero: "L5",
            libelle: "Still Me",
            categorie: {label: "Health & Fitness", key: "Health&Fitness"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Katherine Center",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/still_me.png'),
            disponible: false
        },
        {
            numero: "L6",
            libelle: "The Best book in 2018",
            categorie: {label: "Art et music", key: "Art&music"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Author",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/the_best_book.png'),
            disponible: false
        },
        {
            numero: "L7",
            libelle: "The Hate you give",
            categorie: {label: "Horror", key: "Horror"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Angie Thomas",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/the_hate_you_give.png'),
            disponible: false
        },
        {
            numero: "L8",
            libelle: "The sun does shine",
            categorie: {label: "Horror", key: "Horror"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Anthony Ray hitton",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/the_sun.png'),
            disponible: true
        },
        {
            numero: "L9",
            libelle: "Us against you",
            categorie: {label: "Horror", key: "Horror"},
            description: "",
            date_publication: "2019-02-01",
            auteur: "Frederik Backman",
            edition: "A novel",
            page: 260,
            image_path: require('../../assets/images/user.png'),
            disponible: true
        },
    ],
    categories: [
        {label: "Art et music", key: "Art&music"},
        {label: "Biographies", key: "Biographies"},
        {label: "Business", key: "Business"},
        {label: "Kids", key: "Kids"},
        {label: "Comics", key: "Comics"},
        {label: "Computer & Tech", key: "Computer&Tech"},
        {label: "Cooking", key: "Cooking"},
        {label: "Hobbies & Craft", key: "Hobbies&Craft"},
        {label: "Education & Reference", key: "Edu&Reference"},
        {label: "Health & Fitness", key: "Health&Fitness"},
        {label: "History", key: "History"},
        {label: "Home & Garden", key: "Home&Garden"},
        {label: "Horror", key: "Horror"},
        {label: "Entertainment", key: "Entertainment"},
    ]
};

const pretReducer = (state = initialState, action) => {
    let prets = [...state.prets];
    let livres = [...state.livres];
    let newState;
    switch (action.type) {
        case ADD_PRET:
            prets.push(action.payload);
            newState = {...state, prets};
            return newState;
            break;
        case UPDATE_PRET:
            const {payload} = action;
            let index = state.prets.findIndex(pret => pret.membre.numero === payload.membre.numero && pret.date_pret === payload.date_pret);

            newState = {...state};
            return newState;
            break;
        case DELETE_PRET:
            let selected = action.payload;
            let temp = state.prets;
            selected.map((s, i) => {
                temp = temp.filter(p => {
                    return p.membre.numero !== s.membre.numero || p.date_pret !== s.date_pret
                });
            });
            prets = temp;

            selected.map(pret => {
                pret.livres.map(livre => {
                    let index = livres.findIndex(l => livre.numero === l.numero);
                    livres[index].disponible = true;
                });
            });
            newState = {...state, prets, livres};
            return newState;
            break;
        default:
            return state;
    }
};

export default pretReducer;