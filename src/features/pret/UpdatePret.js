import React from "react";
import {
    Body,
    Button,
    Container,
    Content,
    DatePicker,
    Form,
    Header,
    Icon,
    Item,
    Left, List, ListItem,
    Picker,
    Right,
    Title
} from "native-base";
import {Image, Text, TouchableOpacity} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {addPret} from "./prets";
import {connect} from "react-redux";
import {updateLivre} from "../livre/livres";
import {updatePret} from "../pret/prets";

class UpdatePret extends React.Component {
    constructor(props) {
        super(props);
        let pret = this.props.prets.find(p => p.selected === true);
        this.state = {
            pret: pret,
            chosenDate: pret.date_pret.toString(),
            membre: "",
            selectMode: true,
            canSubmit: false
        };
        this.setDate = this.setDate.bind(this);
        this.props.livres.map((livre, index) => {
            livre.selected = this.state.pret.livres.findIndex(p => p.numero === livre.numero) !== -1 ? true : false;
            console.log(livre.selected);
        });
    }

    onMembreChange(membre) {
        this.setState({membre});
    }

    _pressLivre(index) {
        let selectMode = this.state.selectMode;
        if (selectMode) {
            let _livres = [...this.props.livres];
            if (_livres[index].disponible || _livres[index].selected) {
                _livres[index].selected = _livres[index].selected ? false : true;
                let count = this.selectedLivres(_livres);
                let state = {};
                if (!this.watch()) {
                    state.canSubmit = false
                } else {
                    state.canSubmit = true
                }
                this.setState(state);
            }
        }
    }

    watch() {
        return this.props.livres.filter(l => l.selected === true).length > 0 && this.state.chosenDate !== '';
    }

    selectedLivres(livres) {
        return livres.filter(livre => livre.selected === true).length;
    }

    setDate(newDate) {
        this.setState({
            chosenDate: newDate
        });
        let canSubmit = this.watch();
        if (canSubmit) this.setState({canSubmit});
    }

    componentDidMount(): void {
    }

    clearSelection() {
        let _livres = [...this.props.livres];
        _livres.map(livre => {
            if (livre.selected) livre.selected = false;
        });
    }

    preter() {

        let membre = this.state.membre;
        let date_pret = this.state.chosenDate;

        let oldLivres = this.state.pret.livres;

        let livres = this.props.livres.filter(livre => livre.selected === true);

        this.props.set_disponible(oldLivres, true);

        let pret = this.state.pret;
        pret.livres = livres;

        this.props.set_disponible(livres, false);

        this.props.update_pret(pret);
        this._navigatePret();
    }

    _navigatePret() {
        this.props.navigation.navigate('ListPret');
    }

    render() {
        console.log(this.state.membre);
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('ListPret')}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Modification du pret</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            disabled={!this.state.canSubmit}
                            onPress={() => this.preter()}
                        >
                            <Icon name='ios-checkmark'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                        <Item padder>
                            <Left>
                                <Text style={{fontSize: 18, fontWeight: 'bold', padding: 10}}>
                                    Date pret
                                </Text>
                            </Left>
                            <Body></Body>
                            <Right>
                                <Text style={{
                                    fontSize: 18,
                                    padding: 10
                                }}>{this.state.pret.date_pret}</Text>
                            </Right>
                        </Item>
                        <Item padder>
                            <Left>
                                <Text style={{fontSize: 18, fontWeight: 'bold', padding: 10}}>
                                    Lecteur
                                </Text>
                            </Left>
                            <Body></Body>
                            <Right>
                                <Text style={{
                                    fontSize: 18,
                                    padding: 10
                                }}>{this.state.pret.membre.nom} {this.state.pret.membre.prenom}</Text>
                            </Right>
                        </Item>
                    </Form>
                    <List>
                        <ListItem itemDivider>
                            <Left>
                                <Text style={{color: '#c41d56'}}>Toutes les livres</Text>
                            </Left>
                        </ListItem>
                        <ListItem style={{flexWrap: "wrap", justifyContent: "center"}}>
                            {this.props.livres.map((livre, index) => {
                                    return (
                                        <TouchableOpacity key={index}
                                                          onPress={() => this._pressLivre(index)}
                                                          onLongPress={() => this._longPressLivre(index)}
                                                          style={{
                                                              padding: 0, justifyContent: 'center',
                                                              borderColor: livre.selected ? '#008000' : "",
                                                              opacity: livre.disponible || livre.selected ? 1 : 0.1,
                                                              borderWidth: livre.selected ? 2 : 0,
                                                              margin: 4,
                                                          }}
                                        >
                                            <Image source={livre.image_path} style={{width: wp('27%'), height: wp('32%')}}/>
                                            <Text style={{
                                                fontSize: 14,
                                                overflow: "hidden",
                                                color: "black",
                                                width: wp('27%')
                                            }}>{livre.libelle}</Text>
                                        </TouchableOpacity>
                                    )
                                }
                            )}
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
        membres: state.membres.membres,
        prets: state.prets.prets
    };
};

const mapDispatchToProps = dispatch => {
    return {
        add_pret: (pret) => {
            dispatch(addPret(pret));
        },
        update_pret: (pret) => {
            dispatch(updatePret(pret));
            // dispatch update livre
        },
        set_disponible: (livres: array, value) => {
            livres.map(livre => {
                livre.disponible = value;
                dispatch(updateLivre(livre));
            })
        },
        set_disponibleLivre: (livre, value) => {
            livre.disponible = value;
            dispatch(updateLivre(livre));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePret);