import {ADD_PRET, UPDATE_PRET, DELETE_PRET} from './types';

const addPret = pret => {
    return {
        type: ADD_PRET,
        payload: pret
    }
};

const updatePret = pret => {
    return {
        type: UPDATE_PRET,
        payload: pret
    }
};

const deletePret = prets => {
    return {
        type: DELETE_PRET,
        payload: prets
    }
};

export {
    addPret,updatePret,deletePret
};