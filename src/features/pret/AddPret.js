import React from "react";
import {
    Body,
    Button,
    Container,
    Header,
    Icon,
    Left,
    Right,
    Title,
    Content,
    Item,
    Picker,
    Form,
    DatePicker, ListItem, List
} from "native-base";
import {deleteLivre, updateLivre} from "../livre/livres";
import {addPret} from "./prets";
import {connect} from "react-redux";
import {Image, Text, TouchableOpacity} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

class AddPret extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            membre: null,
            chosenDate: '',
            selectMode: true,
            canSubmit: false
        };
        this.setDate = this.setDate.bind(this);
    }

    componentDidMount(): void {
        this.setState({
            membre: this.props.membres[0],
            chosenDate: '',
            selectMode: true,
            canSubmit: false
        });
        this.clearSelection();
    }

    watch() {
        return this.props.livres.filter(l => l.selected === true).length > 0 && this.state.chosenDate !== '';
    }

    clearSelection(){
        let _livres = [...this.props.livres];
        _livres.map(livre => {
            if (livre.selected) livre.selected = false;
        });
    }

    setDate(newDate) {
        this.setState({
            chosenDate: newDate
        });
        let canSubmit = this.watch();
        if(canSubmit) this.setState({canSubmit});
    }

    onMembreChange(membre) {
        this.setState({membre});
    }

    selectedLivres(livres) {
        return livres.filter(livre => livre.selected === true).length;
    }

    preter() {
        let livres = this.props.livres.filter(livre => livre.selected === true);
        let membre = this.state.membre;
        let date_pret = this.state.chosenDate;
        let pret = {
            livres: livres,
            membre: membre,
            date_pret: date_pret,
            disponible: false
        };
        this.props.add_pret(pret);
        this.props.set_disponible(pret.livres, false);
        this._navigatePret();
    }

    _navigatePret() {
        this.props.navigation.navigate('ListPret');
    }

    _pressLivre(index) {
        let selectMode = this.state.selectMode;
        if (selectMode) {
            let _livres = [...this.props.livres];
            if(_livres[index].disponible) {
                _livres[index].selected = _livres[index].selected ? false : true;
                let count = this.selectedLivres(_livres);
                let state = {};
                if (!this.watch()) {
                    state.canSubmit = false
                } else {
                    state.canSubmit = true
                }
                this.setState(state);
            }
        }
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={()=> this.props.navigation.navigate('ListPret')}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Ajouter un nouveau pret</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            disabled={!this.state.canSubmit}
                            onPress={()=> this.preter()}
                        >
                            <Icon name='ios-checkmark'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                        <Item>
                            <DatePicker
                                defaultDate={new Date()}
                                minimumDate={new Date(1990, 1, 1)}
                                maximumDate={new Date(2050, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Date de prêt"
                                textStyle={{color: "black"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                        <Item>
                            <Picker
                                mode="dropdown"
                                style={{}}
                                placeholder="Selectionnez le membre"
                                placeholderStyle={{color: "#bfc6ea"}}
                                placeholderIconColor="#007aff"
                                selectedValue={this.state.membre}
                                onValueChange={this.onMembreChange.bind(this)}
                            >
                                {this.props.membres.map((item, index) =>
                                    <Picker.Item label={item.nom + ' ' + item.prenom} value={item}
                                                 key={item.numero}/>
                                )}

                            </Picker>
                        </Item>
                    </Form>
                    <List>
                        <ListItem itemDivider>
                            <Left>
                                <Text style={{color: '#c41d56'}}>Toutes les livres</Text>
                            </Left>
                        </ListItem>
                        <ListItem style={{flexWrap: "wrap", justifyContent: "center"}}>
                            {this.props.livres.map((livre, index) => (
                                    <TouchableOpacity key={index}
                                                      onPress={() => this._pressLivre(index)}
                                                      style={{
                                                          padding: 0, justifyContent: 'center',
                                                          borderColor: livre.selected ? '#008000' : "",
                                                          opacity: livre.disponible ? 1 : 0.1,
                                                          borderWidth: livre.selected ? 2 : 0,
                                                          margin: 4,
                                                      }}
                                    >
                                        <Image source={livre.image_path} style={{width: wp('27%'), height: wp('32%')}}/>
                                        <Text style={{ fontSize: 14, overflow: "hidden",color: "black", width: wp('27%')}}>{livre.libelle}</Text>
                                    </TouchableOpacity>
                                )
                            )}
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        livres: state.livres.livres,
        membres: state.membres.membres
    };
};

const mapDispatchToProps = dispatch => {
    return {
        add_pret: (pret) => {
            dispatch(addPret(pret));
        },
        set_disponible: (livres: array, value) => {
            livres.map(livre => {
                livre.disponible = value;
                dispatch(updateLivre(livre));
            })
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPret);