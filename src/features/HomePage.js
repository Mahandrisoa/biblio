import React from "react";
import {
    ImageBackground,
    View,
    TouchableHighlight,
    StyleSheet
} from "react-native";
import {
    Left,
    Right,
    Badge,
    Text,
    Content,
    Body
} from "native-base";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {deletePret} from "./pret/prets";
import {connect} from "react-redux";

class HomePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground source={require('../assets/images/fond_accueil.png')}
                             style={{width: '100%', height: '100%'}}>
                <Content>
                    <View style={styles.menuTop}>
                        <Text style={{height: hp('10%'), fontSize: 25, fontWeight: 'bold', textAlign: 'center', color: 'white'}}>Gestion Bibliothècaire</Text>
                    </View>
                    <View>
                        <TouchableHighlight style={styles.menuList} onPress={()=>{ this.props.navigation.navigate('ListLivre')}}>
                            <View style={styles.menuListContent}>
                                <Text style={styles.menuText}>Liste des ouvrages</Text>
                                <Badge>
                                    <Text>
                                        {this.props.livres_count}
                                    </Text>
                                </Badge>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.menuList} onPress={()=>{ this.props.navigation.navigate('ListMember')}}>
                            <View style={styles.menuListContent}>
                                <Text style={styles.menuText}>Liste des membres</Text>
                                <Badge>
                                    <Text>
                                        {this.props.membres_count}
                                    </Text>
                                </Badge>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.menuList} onPress={()=>{ this.props.navigation.navigate('ListPret')}}>
                            <View style={styles.menuListContent}>
                                <Text style={styles.menuText}>Prets</Text>
                                <Badge>
                                    <Text>
                                        {this.props.prets_count}
                                    </Text>
                                </Badge>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.menuList} onPress={()=>{ this.props.navigation.navigate('Situation')}}>
                            <View style={styles.menuListContent}>
                                <Text style={styles.menuText}>Situation des livres</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </Content>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    menuTop: {
        height: hp('50%'),
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    menuTopFeatures:{
        height: hp('10%') ,
        flexDirection:'row',
        justifyContent:'center',
    },
    menuTopText: {
        color: '#f3ffd9',
        fontSize: 18,
        textAlignVertical: 'center',
        padding: 10
    },
    menuTopTextCenter :{
        color: '#f3ffd9',
        fontSize:25,
        textAlignVertical: 'center',
        padding: 10
    },
    menuBottom: {},
    menuList: {
        height: hp('10%'),
        justifyContent: 'center'
    },
    menuListContent: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 30,
        paddingRight: 30,
    },
    menuText: {
        color: '#f3ffd9',
        fontSize: 20,
        fontWeight: 'bold',
        textShadowOffset: {
            width: -1,
            height: 1
        },
        textShadowRadius: 10
    }
});

const mapStateToProps = state => {
    return {
        prets_count: state.prets.prets.length,
        livres_count: state.livres.livres.length,
        membres_count: state.membres.membres.length
    };
};

const mapDispatchToProps = dispatch => {
    return {
        delete: (prets: array) => {
            dispatch(deletePret(prets));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);