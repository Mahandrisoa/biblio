import {ADD_MEMBRE, UPDATE_MEMBRE, DELETE_MEMBRE} from './types';

const addMembre = membre => {
    return {
        type: ADD_MEMBRE,
        payload: membre
    }
};

const updateMembre = membre => {
    return {
        type: UPDATE_MEMBRE,
        payload: membre
    }
};

const deleteMembre = membres => {
    return {
        type: DELETE_MEMBRE,
        payload: membres
    }
};

export {
    addMembre,
    updateMembre,
    deleteMembre
};