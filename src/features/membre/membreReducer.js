import {ADD_MEMBRE, UPDATE_MEMBRE, DELETE_MEMBRE} from "./types";

const initialState = {
    membres: [
        {
            nom: "Richester",
            prenom: "Fabrice",
            numero: "M1",
            date_naissance:"30-07-1997",
            description : "Aucune",
            avatar: require('../../assets/images/fabrice.png')
        },
        {
            nom: "Sharon",
            prenom: "Alice",
            numero: "M2",
            date_naissance:"30-07-1997",
            description : "Aucune",
            avatar: require('../../assets/images/alice.jpg')
        },
        {
            nom: "Beaulieu",
            prenom: "Didier",
            numero: "M3",
            date_naissance:"30-07-1997",
            description : "Aucune",
            avatar: require('../../assets/images/didier.jpg')
        },
        {
            nom: "Francis",
            prenom: "Antoine",
            numero: "M4",
            date_naissance:"30-07-1997",
            description : "Aucune",
            avatar: require('../../assets/images/antoine.jpg')
        }
    ]
};

const membreReducer = (state = initialState, action) => {
    let membres = [...state.membres];
    let newState;
    switch (action.type) {
        case ADD_MEMBRE:
            membres.push(action.payload);
            newState = {...state, membres};
            return newState;
            break;
        case UPDATE_MEMBRE:
            const {numero} = action.payload;
            console.log(numero);
            let index = membres.findIndex((m) => m.numero === numero);
            membres[index] = {...action.payload};
            newState = {...state, membres};
            return newState;
            break;
        case DELETE_MEMBRE:
            const selected = action.payload;
            let temp = state.membres;
            console.log(temp);
            selected.map((s, i) => {
                temp = temp.filter(m => m.numero !== s.numero);
            });
            membres = temp;
            newState = {...state, membres};
            return newState;
            break;
        default:
            return state;
    }
};

export default membreReducer;