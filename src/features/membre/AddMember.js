import React from "react";
import {TextInput} from "react-native";
import {
    Body,
    Button,
    Container,
    Header,
    Content,
    Text,
    Icon,
    Left,
    Right,
    Title,
    Form,
    Item,
    Input,
    DatePicker,
    Textarea
} from "native-base";
import {addMembre} from "./membres";
import {connect} from "react-redux";

class AddMember extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            numero: '',
            nom: '',
            prenom: '',
            date_naissance: new Date(),
            description: '',
            avatar: require('../../assets/images/user.png')
        };
        this.setDate = this.setDate.bind(this);
    }

    setDate(date_naissance) {
        this.setState({date_naissance});
    }

    submitForm() {
        let membre = {numero, nom, prenom, description, avatar} = this.state;
        membre.numero = `L${this.props.membres.length + 1}`;
        this.props.add(membre);
        this.props.navigation.goBack();
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Créer un nouveau membre</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.submitForm()}>
                            <Icon name='ios-checkmark'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                        <Item>
                            <Input placeholder="Nom"
                                   onChangeText={(nom) => this.setState({nom})}/>
                        </Item>
                        <Item last>
                            <Input placeholder="Prénom(s)"
                                   onChangeText={(prenom) => this.setState({prenom})}/>
                        </Item>
                        <Item>
                            <DatePicker
                                defaultDate={new Date()}
                                minimumDate={new Date(2018, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Date de naissance"
                                textStyle={{color: "black"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                        <Item>
                            <Textarea rowSpan={5} placeholder="Description ... " style={{width: '100%'}}
                                      onChangeText={(description) => this.setState({description})}/>
                        </Item>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        membres: state.membres.membres,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        add: membre => {
            dispatch(addMembre(membre));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddMember);

