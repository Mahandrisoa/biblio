import React from "react";
import {
    Body,
    Button,
    Container,
    Content,
    DatePicker,
    Form,
    Header,
    Icon,
    Input,
    Item,
    Left,
    Right, Textarea,
    Title
} from "native-base";
import {updateMembre} from "./membres";
import {connect} from "react-redux";

class UpdateMember extends React.Component {
    constructor(props) {
        super(props);
        const membre = this.props.membres.find(l => l.selected); // get selected book
        console.log(membre);
        this.state = membre; // set initial state
    }

    setDate(date_naissance) {
        this.setState({date_naissance});
    }

    navigateTo(route: string) {
        this.props.navigation.navigate(route);
    }

    submitForm() {
        const membre = {numero, nom, prenom, description, date_naissance} = this.state;
        this.props.update(membre);
        this.navigateTo('ListMember');
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Modifier le membre</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.submitForm()}>
                            <Icon name='ios-checkmark'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                        <Item>
                            <Input placeholder="Nom"
                                   value={this.state.nom}
                                   onChangeText={(nom) => this.setState({nom})}/>
                        </Item>
                        <Item last>
                            <Input placeholder="Prénom(s)"
                                   value={this.state.prenom}
                                   onChangeText={(prenom) => this.setState({prenom})}/>
                        </Item>
                        <Item>
                            <DatePicker
                                defaultDate={new Date(this.state.date_naissance)}
                                minimumDate={new Date(2018, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Date de naissance"
                                textStyle={{color: "black"}}
                                placeHolderTextStyle={{color: "#d3d3d3"}}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                        </Item>
                        <Item>
                            <Textarea
                                value={this.state.description}
                                rowSpan={5}
                                placeholder="Description ... "
                                style={{width: '100%'}}
                                onChangeText={(description) => this.setState({description})}/>
                        </Item>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        membres: state.membres.membres,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        update: membre => {
            dispatch(updateMembre(membre));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateMember);