import React, {Component} from "react";
import {ImageBackground, StyleSheet, View} from "react-native";
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Left,
    Body,
    Right,
    Thumbnail,
    Text,
    Icon,
    Fab,
    Button, Title
} from 'native-base';
import {deleteMembre} from "./membres";
import {connect} from "react-redux";
import moment from "moment";

class ListMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            canEdit: false,
            canDelete: false,
            selectMode: false,
            isModalVisible: false,
            active: true,
        };
    }

    _longPressMembre(index) {
        let _membres = [...this.props.membres];
        _membres[index].selected = true;
        let count = this.selectedMembres(_membres);
        let obj = {
            selectMode: true
        };
        if (count === 1) {
            obj.canEdit = true;
        } else {
            if (count > 1) {
                obj.canEdit = false;
            }
        }
        this.setState(obj);
    }

    _pressMembre(index) {
        let selectMode = this.state.selectMode;
        if (selectMode) {
            let _membres = [...this.props.membres];
            _membres[index].selected = _membres[index].selected ? false : true;
            let count = this.selectedMembres(_membres);
            let obj = {};
            if (count === 1) {
                obj.canEdit = true;
            } else {
                if (count > 1) {
                    obj.canEdit = false;
                }
            }
            this.setState(obj);
        } else {
            this.props.navigation.navigate('ShowMember', {index: index});
        }
    }

    selectedMembres(membres) {
        return membres.filter(membre => membre.selected === true).length;
    }

    _deleteMembres() {
        const membres = this.props.membres.filter(m => m.selected === true);
        this.props.delete(membres);
        this._cancelSelection();
    }

    _cancelSelection() {
        let _membres = [...this.props.membres];
        _membres.map(membre => {
            if (membre.selected) membre.selected = false;
        });
        this.setState({
            membres: _membres,
            selectMode: false
        });
    }

    gotoUpdate() {
        this.props.navigation.navigate('UpdateMember');
    }

    _navigateHome() {
        this.props.navigation.navigate('HomePage');
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/images/fond_list_livre.png')}
                             style={styles.backgroundImage}>
                <Container>
                    {!this.state.selectMode &&
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this._navigateHome()}>
                                <Icon name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                            <Title>Liste des membres</Title>
                        </Body>
                        <Right>
                            <Button transparent>
                                <Icon name='search'/>
                            </Button>
                        </Right>
                    </Header>
                    }
                    {this.state.selectMode &&
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this._cancelSelection()}>
                                <Icon name='ios-close'/>
                            </Button>
                        </Left>
                        <Body>
                            <Title>Liste des membres</Title>
                        </Body>
                        <Right>
                            {this.state.canEdit &&
                            <Button transparent onPress={() => this.gotoUpdate()}>
                                <Icon name='ios-color-palette'/></Button>}
                            <Button transparent onPress={() => this._deleteMembres()}>
                                <Icon name='trash'/>
                            </Button>
                        </Right>
                    </Header>}
                    <Content>
                        <List>
                            {this.props.membres.map((membre, index) => (
                                <ListItem avatar key={index}
                                          onLongPress={() => this._longPressMembre(index)}
                                          onPress={() => this._pressMembre(index)}
                                          noIndent
                                          style={{backgroundColor: membre.selected ? '#cde1f9' : '#fff'}}
                                >
                                    <Left>
                                        <Thumbnail source={membre.avatar}/>
                                    </Left>
                                    <Body>
                                        <Text>{membre.nom} {membre.prenom}</Text>
                                        <Text note>{membre.date_naissance.toString()}</Text>
                                    </Body>
                                    <Right>
                                        <Text note>{membre.numero}</Text>
                                    </Right>
                                </ListItem>
                            ))}
                        </List>

                    </Content>
                    {!this.state.selectMode &&
                    <Fab
                        active={true}
                        direction="up"
                        containerStyle={{}}
                        style={{backgroundColor: '#5067FF'}}
                        position="bottomRight"
                        onPress={() => this.props.navigation.navigate('AddMember')}>
                        <Icon name="add"/>
                    </Fab>
                    }
                </Container>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: '100%',
        height: '100%'
    }
});

const mapStateToProps = state => {
    return {
        membres: state.membres.membres,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        delete: membres => {
            dispatch(deleteMembre(membres));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListMember);