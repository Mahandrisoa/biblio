import React from "react";
import {
    ImageBackground,
    View,
    TouchableHighlight,
    ScrollView, TouchableOpacity, StyleSheet
} from "react-native";
import {
    Left,
    Right,
    Text,
    Container,
    Body,
    Content,
    Header, Button, Icon, H3, Thumbnail, Title
} from "native-base";

import {deleteMembre} from "./membres";
import {connect} from "react-redux";
import Table from "react-native-simple-table";
import {heightPercentageToDP as hp, wp} from "react-native-responsive-screen";

class ShowMember extends React.Component {
    constructor(props) {
        super(props);
        const index = this.props.navigation.getParam('index');
        this.state = {
            membre: this.getMembre(index),
            prets: this.generatePrets(index)
        };
    }

    getMembre(index) {
        return this.props.membres[index];
    }

    generatePrets(index) {
        const prets = this.props.prets.filter(p => p.membre.numero === this.props.membres[index].numero);
        return prets;
    }

    countBooks(prets) {
        let som = 0;
        prets.map(p => som += p.livres.length); // due to multiple books
        return som;
    }

    _navigateHome() {
        this.props.navigation.navigate('HomePage');
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('ListMember')}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Detail du membre</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='search'/>
                        </Button>
                    </Right>
                </Header>
                <ScrollView>
                    <View style={{
                        margin: 10,
                        borderRadius: 8,
                        alignSelf: 'center',
                        padding: 20,
                        width: '95%',
                        backgroundColor: 'rgba(188,195,203,0.5)'
                    }}>
                        <Left>
                            <Thumbnail source={this.state.membre.avatar} large/>
                        </Left>
                        <Right>
                            <Text style={{margin: 10}}>
                                {this.state.membre.nom} {this.state.membre.prenom}
                            </Text>
                        </Right>
                        <View style={{flexDirection: 'row'}}>
                            <Left><Text style={{fontWeight: 'bold'}}>N° Lecteur</Text></Left>
                            <Body><Text>{this.state.membre.numero}</Text></Body>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Left><Text style={{fontWeight: 'bold'}}>Description</Text></Left>
                            <Body><Text>{this.state.membre.description}</Text></Body>
                        </View>
                    </View>
                    <View style={{
                        margin: 10,
                        padding: 20,
                        borderRadius: 8,
                        flexDirection: 'column',
                        width: '95%',
                        alignSelf: 'center',
                        backgroundColor: 'rgba(188,196,203,0.5)'
                    }}>
                        <Text>
                            <H3>{this.countBooks(this.state.prets)} Livres prétés</H3>
                        </Text>
                        {this.state.prets.map((pret, index) => (
                            <View key={index}>
                                <View>
                                    <Text style={{right: 0, color: '#c41d56', margin: 5}}>
                                        {pret.date_pret}
                                    </Text>
                                </View>
                                <ScrollView horizontal>
                                    <View style={{
                                        width: "100%",
                                        flexDirection: "row",
                                        flexWrap: "wrap",
                                        justifyContent: "flex-start"
                                    }}>
                                        {pret.livres.map((livre, index) => (
                                                <TouchableOpacity key={index}
                                                                  style={{margin: 10, padding: 0}}
                                                >
                                                    <Thumbnail square large source={livre.image_path}/>
                                                </TouchableOpacity>
                                            )
                                        )}
                                    </View>
                                </ScrollView>
                            </View>
                        ))
                        }
                    </View>
                    <View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        membres: state.membres.membres,
        livres: state.livres.livres,
        prets: state.prets.prets
    };
};

const mapDispatchToProps = dispatch => {
    return {
        delete: membres => {
            dispatch(deleteMembre(membres));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowMember);