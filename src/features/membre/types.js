const ADD_MEMBRE = "ADD_MEMBRE";
const UPDATE_MEMBRE = "UPDATE_MEMBRE";
const DELETE_MEMBRE = "DELETE_MEMBRE";

export {
    ADD_MEMBRE,
    UPDATE_MEMBRE,
    DELETE_MEMBRE
};