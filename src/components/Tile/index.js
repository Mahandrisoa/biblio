import React from "react";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {View, StyleSheet} from "react-native";
import {Text} from "native-base";

export default class Tile extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.tile}>
                <Text style={styles.count}>{this.props.count}</Text>
                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tile: {
        width: wp('30%'),
        height: hp('30%'),
        backgroundColor: 'rgba(188,196,203,0.8)',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    count: {
        color: '#c41d56',
        fontSize: 22
    },
    label: {}
});
