import React from "react";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {ImageBackground, StyleSheet, View} from "react-native";
import {Button, Header, Icon, Left, Text,Content, Thumbnail} from "native-base";

export default class Profil extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground source={this.props.cover_photo}
                             style={styles.photo_cover}>
                <Header transparent searchbar>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                </Header>
                <Content>
                    <View style={{
                        flexDirection: 'row',
                    }}>
                        <View style={{
                            textAlignVertical: 'center',
                            textAlign: 'center',
                            flexDirection: 'column'
                        }}>
                            <Text style={{fontSize: 22, color: '#fff'}}>{this.props.full_name}</Text>
                            <Text style={{fontSize: 18, color: '#fff'}}>{this.props.location}</Text>
                        </View>
                    </View>
                </Content>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    photo_cover: {
        width: '100%',
        height: 320
    },
    count: {
        color: '#c41d56',
        fontSize: 22
    },
    label: {}
});
