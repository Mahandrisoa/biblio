import React from "react";
import {Image, Text, View, StyleSheet} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class Etagere extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const categorie = this.props.categorie;
        return (
            <View style={styles.etagere}>
                <Text style={styles.categorie_text}>{categorie}</Text>
                <View style={styles.container}>
                    <Image style={styles.livre}
                           source={require('../../assets/images/livre.png')} />
                    <Image style={styles.barre}
                           source={require('../../assets/images/etagere.png')}/>
                </View>
            </View>
        );
    }
}
const TEXT_LENGTH = hp('35%');
const TEXT_HEIGHT = 25;
const OFFSET = TEXT_LENGTH / 2 - TEXT_HEIGHT / 2;

const styles = StyleSheet.create({
    etagere: {
        height: hp('35%'),
        flexDirection: 'column'
    },
    categorie_text: {
        transform: [{rotate: '-90deg'},
            {translateX: -OFFSET},
            {translateY: OFFSET}
        ],
        color: '#C41D56',
        fontSize: 22,
        textAlign: 'center',
        height: TEXT_HEIGHT,
        width: TEXT_LENGTH,
        left: -200
    },
    container: {
        height: hp('35%'),
        width: wp('100%')
    },
    barre: {
        width: '100%',
        bottom: hp('-23%'),
        height: 60,
        position: 'relative'
    },
    livre: {
        width: 200,
        height: 200,
    }
});