import React, {Component} from 'react';
import {TouchableHighlight, Image, StyleSheet, ImageBackground, FlatList} from 'react-native';
import {Content, Text, View, ListItem, Icon, Container, Left, Right, Badge} from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const datas = [
    //{image: require('../../assets/icons/icon-recette.png'), navigateTo: 'ListRecette', textLabel: 'Recettes'},
    {image: require('../../assets/icons/icon-recette.png'), navigateTo: 'Bot', textLabel: 'Bot'},
    {image: require('../../assets/icons/icon-régime.png'), navigateTo: 'ListRegime', textLabel: 'Régimes'},
    {image: require('../../assets/icons/icon-eau.png'), navigateTo: 'Eau', textLabel: 'Rappel d\'eau'},
    {image: require('../../assets/icons/icon-photo.png'), navigateTo: 'Photo', textLabel: 'Reconnaissance photo'},
]

class SideBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground
                source={require('../../assets/images/darkgreen.jpg')}
                style={{width: '100%', height: '100%', margin: 0, flexDirection: 'column'}}
            >
                <FlatList
                    data={datas}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) =>
                        <TouchableHighlight

                            onPress={() => {
                                this.props.navigation.closeDrawer();
                                this.props.navigation.navigate(item.navigateTo)
                            }}
                            style={styles.item}
                        >
                            <View>
                                <Image
                                    source={item.image}
                                    style={styles.itemImage}
                                />
                                <Text
                                    style={styles.textLabel}
                                >{item.textLabel}</Text>
                            </View>
                        </TouchableHighlight>
                    }
                />
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'rgba(255,255,255,0.6)',
        borderBottomWidth: 1,
        padding: 10
    },
    itemImage: {
        margin: 0,
        opacity: 0.8
    },
    textLabel: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'rgba(185,185,185,0.58)',
    }
});

export default SideBar;