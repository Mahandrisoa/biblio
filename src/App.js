import React from 'react';

import {createStackNavigator, createAppContainer} from 'react-navigation';

import HomePage from "./features/HomePage";
import ListLivre from "./features/livre/ListLivre";
import UpdateLivre from "./features/livre/UpdateLivre";
import AddLivre from "./features/livre/AddLivre";
import ShowLivre from "./features/livre/ShowLivre";
import ListMember from "./features/membre/ListMember";
import AddMember from "./features/membre/AddMember";
import ShowMember from "./features/membre/ShowMember";
import UpdateMember from "./features/membre/UpdateMember";
import ListPret from "./features/pret/ListPret";
import AddPret from "./features/pret/AddPret";
import UpdatePret from "./features/pret/UpdatePret";
import Chart from "./features/chart/Chart";
import Situation from "./features/livre/Situation";


const AppNavigator = createStackNavigator({
    HomePage: {screen: HomePage},
    ListLivre: {screen: ListLivre},
    AddLivre: {screen: AddLivre},
    UpdateLivre: {screen: UpdateLivre},
    ShowLivre: {screen: ShowLivre},
    ListMember: {screen: ListMember},
    AddMember: {screen: AddMember},
    ShowMember: {screen: ShowMember},
    UpdateMember: {screen: UpdateMember},
    ListPret: {screen: ListPret},
    AddPret: {screen: AddPret},
    UpdatePret: {screen: UpdatePret},
    Chart: {screen: Chart},
    Situation: {screen: Situation}
}, {
    initialRouteName: 'HomePage',
    headerMode: 'none'
});

const Navigator = createAppContainer(AppNavigator);
export default Navigator;