const state = {
    membres: [
        {numero: 'm1'},
        {numero: 'm2'},
        {numero: 'm3'},
    ],
    livres: [],
    categories: [
        {label: "Art et music", key: "Art&music"},
        {label: "Biographies", key: "Biographies"},
        {label: "Business", key: "Business"},
        {label: "Kids", key: "Kids"},
        {label: "Comics", key: "Comics"},
        {label: "Computer & Tech", key: "Computer&Tech"},
        {label: "Cooking", key: "Cooking"},
        {label: "Hobbies & Craft", key: "Hobbies&Craft"},
        {label: "Education & Reference", key: "Edu&Reference"},
        {label: "Health & Fitness", key: "Health&Fitness"},
        {label: "History", key: "History"},
        {label: "Home & Garden", key: "Home&Garden"},
        {label: "Horror", key: "Horror"},
        {label: "Entertainment", key: "Entertainment"},
    ]
};

export default state;