import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {Root} from "native-base";
import Navigator from "./src/App";
import configureStore from "./src/store";

import { ApolloClient } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';

export default class App extends Component<Props> {
  constructor(props){
    super(props);
  }

  render() {
    const store = configureStore();

    return (
        <Provider store={store}>
          <Root>
            <Navigator/>
          </Root>
        </Provider>
    );
  }
}
